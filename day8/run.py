#!/usr/bin/python3
trees = []
r, c = 0, 0 # how many rows and cols
with open('input') as f:
    rows = f.read().splitlines()
    for row in rows:
        tree_row = []
        for height in row:
            tree_row.append([int(height), False])
        trees.append(tree_row)
    r = len(rows)
    c = len(row)

def view_trees(i,j,m1,m2,m3,m4):
    if i == 0 or j == 0 or i == (r-1) or j == (c-1):
        trees[i][j][1] = True

    # i > and j >
    t = trees[i][j]
    if t[0] > m1:
        t[1] = True
    m1 = max(m1, t[0])
    # i < and j <
    t = trees[r-i-1][c-j-1]
    if t[0] > m2:
        t[1] = True
    m2 = max(m2, t[0])
    # i > and j <
    t = trees[i][c-j-1]
    if t[0] > m3:
        t[1] = True
    m3 = max(m3, t[0])
    # i < and j <
    t = trees[r-i-1][c-j-1]
    if t[0] > m4:
        t[1] = True
    m4 = max(m4, t[0])
    return m1, m2, m3, m4

# part 1
m1, m2, m3, m4 = 0, 0, 0, 0
for i in range(0,r):
    for j in range(0,c):
        m1, m2, m3, m4 = view_trees(i,j,m1,m2,m3,m4)
    m1 = 0
    m2 = 0
    m3 = 0
    m4 = 0

m1, m2, m3, m4 = 0, 0, 0, 0
for j in range(0,c):
    for i in range(0,r):
        m1, m2, m3, m4 = view_trees(i,j,m1,m2,m3,m4)
    m1 = 0
    m2 = 0
    m3 = 0
    m4 = 0

count = 0
for row in trees:
    for t in row:
        if t[1]:
            count += 1
print(count)

# part 2
def score(i, j):
    t = trees[i][j]
    score_l, score_r, score_t, score_b = 0, 0, 0, 0

    # to left
    for k in range(0, j):
        h = trees[i][j-k-1][0]
        if h < t[0]:
            score_l += 1
        elif h >= t[0]:
            score_l += 1
            break

    # to right
    for k in range(j+1, c):
        h = trees[i][k][0]
        if h < t[0]:
            score_r += 1
            th = h
        elif h >= t[0]:
            score_r += 1
            break

    # to top
    for k in range(0, i):
        h = trees[i-k-1][j][0]
        if h < t[0]:
            score_t += 1
        elif h >= t[0]:
            score_t += 1
            break

    # to bottom
    for k in range(i+1, r):
        h = trees[k][j][0]
        if h < t[0]:
            score_b += 1
        elif h >= t[0]:
            score_b += 1
            break

    return (score_l * score_r * score_t * score_b)

i, j, m = 0, 0, 0
for i in range(1,r-1):
    for j in range(1,c-1):
        m = max(m,(score(i,j)))
print(m)
