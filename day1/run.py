#!/usr/bin/python3
file = open("input", "r")

elves, calories = [], 0
for line in file:
	if line.strip() == "":
		elves.append(calories)
		calories = 0
	else:
		calories += int(line.strip())
elves.sort()

print(elves[-1])
print(sum(elves[-3:]))
