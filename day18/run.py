#!/usr/bin/env python3
from collections import defaultdict
import itertools

d = defaultdict(int)
with open('input') as f:
    lines = f.read().splitlines()
    for l in lines:
        x,y,z = l.split(',')
        d[(int(x),int(y),int(z))] = 1

# part 1
res_part1 = 0
items = list(d.items())
for p,v in items: # for each point
    for i in [(1,0,0),(-1,0,0),(0,1,0),(0,-1,0),(0,0,1),(0,0,-1)]: # in all directions
        if not d[(p[0]+i[0],p[1]+i[1],p[2]+i[2])]:
            res_part1 += 1
print(res_part1)

# part 2
n = 20
# from any point of space, returns True if (0,0,0) can be reached, or if that point is part of the cubes
def is_cube_trapped(c):
    if d[c]:
        return False

    open_list = set()
    open_list.add(c)
    closed_list = set()

    while len(open_list):
        p = None
        for p in open_list:
            break

        dirs = [(1,0,0),(-1,0,0),(0,1,0),(0,-1,0),(0,0,1),(0,0,-1)]
        for i in dirs: # in all directions
            np = (p[0]+i[0],p[1]+i[1],p[2]+i[2])
            if np == (0,0,0):
                return False
            elif d[np]:
                continue
            elif np[0]<=-1 or np[1]<=-1 or np[2]<=-1 or np[0]>n or np[1]>n or np[2]>n:
                continue
            elif not np in closed_list:
                open_list.add(np)
        open_list.remove(p)
        closed_list.add(p)
    return True

trapped_cubes = []
for i,j,k in itertools.product(range(n),range(n),range(n)):
    if is_cube_trapped((i,j,k)):
        trapped_cubes.append((i,j,k))
res_part2 = 0 # surface of trapped cubes
for p in trapped_cubes: # for each trapped cube
    for i in [(1,0,0),(-1,0,0),(0,1,0),(0,-1,0),(0,0,1),(0,0,-1)]: # in all directions
        if d[(p[0]+i[0],p[1]+i[1],p[2]+i[2])]:
            res_part2 += 1
print(res_part1 - res_part2)
