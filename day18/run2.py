#!/usr/bin/env python3
from collections import defaultdict
import sys

d = defaultdict(int)
with open('input') as f:
    lines = f.read().splitlines()
    for l in lines:
        x,y,z = l.split(',')
        d[(int(x),int(y),int(z))] = 1

dirs = [(1,0,0),(-1,0,0),(0,1,0),(0,-1,0),(0,0,1),(0,0,-1)]

# part 1
res_part1 = 0
items = list(d.items())
for p,v in items: # for each point
    for i in dirs: # in all directions
        if not d[(p[0]+i[0],p[1]+i[1],p[2]+i[2])]:
            res_part1 += 1
print(res_part1)

# part 2
n = 20
# flood-fill
sys.setrecursionlimit(10000)
def fill(p):
    d[p] = 2 # 2 means water
    for i in dirs: # in all directions
        np = (p[0]+i[0],p[1]+i[1],p[2]+i[2])
        if np[0]<-1 or np[1]<-1 or np[2]<-1 or np[0]>n or np[1]>n or np[2]>n:
            continue
        elif not d[np]:
            fill(np)
fill((0,0,0))

res_part2 = 0
items = list(d.items())
for p,v in items: # for each point
    if not d[p] == 1:
        continue
    for i in dirs: # in all directions
        if d[(p[0]+i[0],p[1]+i[1],p[2]+i[2])] == 2:
            res_part2 += 1
print(res_part2)
