#!/usr/bin/python3
tree = {}
cur_dir = tree
with open('input') as f:
    lines = f.read().splitlines()
    for line in lines:
        if line.startswith('$'):
            command = line.split(' ')
            if command[1] == 'cd':
                target = command[2]
                if target == '/':
                    pass
                elif target == '..':
                    cur_dir = cur_dir['..']
                else:
                    prev_dir = cur_dir
                    cur_dir = cur_dir[target]
                    cur_dir['..'] = prev_dir
        else: # considering we are reading ls output
            content = line.split(' ')
            if content[0].startswith('dir'):
                cur_dir[content[1]] = {}
            else:
                filesize, filename = content
                cur_dir[filename] = filesize

dir_sizes = []
def parse_tree(tree):    
    size = 0
    for el in tree:
        if el != '..':
            if isinstance(tree[el], dict):
                size += parse_tree(tree[el])
            else:
                size += int(tree[el])
    dir_sizes.append(size)
    return size
parse_tree(tree)


used = dir_sizes[-1]
available = 70000000-used
missing = 30000000-available

answer_part1 = 0
answer_part2 = []
for s in dir_sizes:
    if s <= 100000:
        answer_part1 += s
    if s >= missing:
        answer_part2.append(s)
print(answer_part1)
print(min(answer_part2))
