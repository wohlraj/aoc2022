#!/usr/bin/env python3

# in_file = 'example'
in_file = 'input'

with open(in_file) as f:
    lines = f.read().splitlines()
    data = [(int(x),idx) for idx,x in enumerate(lines)]

def mix(data):
	for i in range(len(data)):
		for k in range(len(data)):
			if data[k][1] == i:
				v,h = data[k]
				break
		data.insert((k+v)%(len(data)-1),data.pop(k))

def solve(data):
	data = [x for x,y in data]
	z = data.index(0)
	print(sum([data[(i+z)%len(data)] for i in [1000, 2000, 3000]]))

# part 1
d = data.copy()
mix(d)
solve(d)

# part 2
data = [(x*811589153,y) for x,y in data]
for _ in range(10):
	mix(data)
solve(data)
