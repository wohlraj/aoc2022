#!/usr/bin/env python3
import numpy as np

with open('rocks') as f:
    lines = f.read().splitlines()
    rocks, rock = [], []
    for l in lines:
        if l == "":
            rocks.append(np.array(rock))
            rock = []
        else:
            l = l.replace('#','1 ')
            l = l.replace('.','0 ')
            rock.append([int(c) for c in l.split()])
    rocks.append(np.array(rock))

with open('input') as f:
    jets = f.read().splitlines()[0]

def move_rock(g, r, d=None):
    # to do: check for collisions with rocks in grid
    if d == "<":
        g = np.rot90(g,1)
        r = np.rot90(r,1)
    elif d == ">":
        g = np.rot90(g,-1)
        r = np.rot90(r,-1)

    # moving down a rock is similar to drawing it one line down in the grid
    # if no direction specified, moving down
    if not(1 in r[-1]):
        rs = np.pad(r, [(1,0),(0,0)], mode='constant') # add empty row at top
        rs = np.delete(rs, len(rs)-1,0) # drop last row
        g2 = g-r+rs
        if not(2 in g2):
            g = g2
        else: # trying to overlap rocks, cancelling move
            rs = r
    else:
        rs = r

    if d == "<":
        g = np.rot90(g,-1)
        rs = np.rot90(rs,-1)
    elif d == ">":
        g = np.rot90(g,1)
        rs = np.rot90(rs,1)

    return g, rs

def solve(n,part2=False):
    # grid
    g = np.array([[0 for _ in range(7)] for _ in range(4)])
    j = 0
    patterns = []
    answers = {}
    for i in range(n):
        if part2:
            # hash, looking for a repeating pattern
            h = [i%len(rocks),j%len(jets)] # rock and jet are part of the hash key
            gh = np.rot90(g)
            for k in range(len(gh)):
                if 1 in list(gh[k]):
                    h.append((list(gh[k]).index(1)))
                else:
                    break
            if len(h) == 9 and h in patterns: # 7-wide grid, plus rock and jet ids
                # getting current height
                while not(1 in g[0]):
                    g = g[1::]
                idx = patterns.index(h)
                idx_sol = answers[idx]
                i_sol=len(g)
                repeating = int((n-idx)/(i-idx))
                repeating_h = i_sol-idx_sol
                missing = n-idx-repeating*(i-idx)
                # res = before_pattern + pattern + after_pattern
                res = idx_sol + repeating*repeating_h + (answers[missing+idx]-idx_sol)
                return res
            patterns.append(h)

        r = rocks[i%len(rocks)] # rock, raw
        # reshaping grid to host rock on top of it
        # removing any empty top row
        while not(1 in g[0]) and i != 0:
            g = g[1:]
        # leaving 3 empty rows at top
        while 1 in g[0:len(r)+3]:
            g = np.pad(g, [(1,0),(0,0)], mode='constant') # adding one row at top
        # reshaping rock to match grid
        rs = np.pad(r, [(0,g.shape[0]-r.shape[0]),(2,g.shape[1]-r.shape[1]-2)], mode='constant')
        g += rs # spawn rock
        while True:
            jet_dir = jets[j%len(jets)]
            j += 1
            g,rs = move_rock(g, rs, jet_dir)
            g_old = g
            g,rs = move_rock(g, rs)
            if np.array_equal(g, g_old):
                break
        while not(1 in g[0]):
            g = g[1::]
        answers[i+1] = len(g)
    return len(g)

print(solve(2022))
print(solve(1_000_000_000_000,part2=True))
