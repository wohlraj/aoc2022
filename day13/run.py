#!/usr/bin/env python3

with open('input') as f:
    l1, l2, data = None, None, []
    for line in f.read().splitlines():
        if l1 == None and line:
            l1 = eval(line)
        elif l2 == None and line:
            l2 = eval(line)
        else:
            data.append([l1,l2])
            l1, l2 = None, None
    data.append([l1,l2])

def compare(a,b):
    # all integers
    if type(a) == int and type(b) == int:
        if a == b:
            return None
        else:
            return a < b
    # all lists
    elif type(a) == list and type(b) == list:
        for aa,bb in zip(a, b):
            cp = compare(aa,bb)
            if cp != None:
                return cp
        return compare(len(a),len(b))
    # mixed types
    elif type(a) == int and not type(b) == int:
        return compare([a],b)
    elif not type(a) == int and type(b) == int:
        return compare(a,[b])

# part 1
res = 0
for i, pair in enumerate(data):
    l1, l2 = pair
    if compare(l1,l2):
        res += i+1
print(res)

# part 2

def insert(d, el=None):
    if el == None:
        return d
    if d == []:
        return [el]
    for x in d:
        if compare(el, x):
            return [el] + insert(d)
        else:
            return [x] + insert(d[1:], el)

data2=[]
for d in data:
    data2.append(d[0])
    data2.append(d[1])
data2.append([[2]])
data2.append([[6]])
res = []
for el in data2:
    res = insert(res, el)

prod = 1
for i, r in enumerate(res):
    if r == [[2]] or r == [[6]]:
        prod *= i+1
print(prod)
