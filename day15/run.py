#!/usr/bin/env python3

beacons = []
sensors = []
dists = []
def dist(a,b):
    return abs(b[0]-a[0])+abs(b[1]-a[1])
with open('input') as f:
    for line in f.read().splitlines():
        sx,sy,bx,by = line.split('=')[1:]
        sx = int(sx.split(',')[0])
        sy = int(sy.split(':')[0])
        bx = int(bx.split(',')[0])
        by = int(by)
        sensors.append([sx,sy])
        beacons.append([bx,by])
        dists.append(dist([sx,sy],[bx,by]))

xy_max = 4_000_000

# stolen from https://www.geeksforgeeks.org/merging-intervals/
def merge_intervals(intervals):
    intervals.sort()
    stack = []
    stack.append(intervals[0])
    for i in intervals[1:]:
        if stack[-1][0] <= i[0] <= stack[-1][-1]+1: # added +1 here for merging contiguous intervals
            stack[-1][-1] = max(stack[-1][-1], i[-1])
        else:
            stack.append(i)
    return stack

# part 1
def find_positions(y, remove_beacons):
    if remove_beacons:
        positions = set()
        for i in range(len(sensors)):
            d = dists[i]
            sx,sy = sensors[i]
            bx,by = beacons[i]
            dist_y = abs(y-sy)
            dist_x = max(0,d-dist_y)
            if dist_x:
                for x in range(sx-dist_x,sx+dist_x+1):
                    positions.add(x)
        for i in range(len(beacons)):
            if beacons[i][1] == y and beacons[i][0] in positions:
                positions.remove(beacons[i][0])
    else:
        position_ranges = []
        for i in range(len(sensors)):
            d = dists[i]
            sx,sy = sensors[i]
            bx,by = beacons[i]
            dist_y = abs(y-sy)
            dist_x = max(0,d-dist_y)
            if dist_x:
                x1,x2 = [max(0,sx-dist_x),min(sx+dist_x,xy_max)]
                if len([x1,x2]):
                    position_ranges.append([x1,x2])
        positions = merge_intervals(position_ranges)
    return positions

y = 2_000_000
print(len(find_positions(y,True)))

# part 2
for y in range(0,xy_max):
    if len(positions := find_positions(y,False)) > 1:
        print(4000000*(positions[0][1]+1)+y)
        break
