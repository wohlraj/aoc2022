#!/usr/bin/python3
import numpy

def distance(a, b):
    return max(abs(b-a))

def move_snake_body(sn):
    h = sn[0]
    t = sn[1]
    if distance(h,t) == 2:
        if h[0] == t[0]: # same x
            t[1] += 1 if (h[1]-t[1]) > 0 else -1
        elif h[1] == t[1]: # same y
            t[0] += 1 if (h[0]-t[0]) > 0 else -1
        else: # diag
            t[1] += 1 if (h[1]-t[1]) > 0 else -1
            t[0] += 1 if (h[0]-t[0]) > 0 else -1
    if len(sn[1::]) >= 2:
        move_snake_body(sn[1::])

def solve(n):
    snake = [numpy.array([0,0]) for _ in range(n)]
    visited = set()
    with open('input') as f:
        rows = f.read().splitlines()
        for row in rows:
            move, val = row.split(' ')
            for _ in range(int(val)):
                if move == 'R':
                    m = (1,0)
                if move == 'L':
                    m = (-1,0)
                if move == 'U':
                    m = (0,1)
                if move == 'D':
                    m = (0,-1)
                snake[0] += m # moved head
                move_snake_body(snake)
                visited.add((snake[-1][0],snake[-1][1]))
    print(len(visited))

solve(2)
solve(10)
