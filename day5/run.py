#!/usr/bin/python3

def answer(stacks):
	msg = ""
	for s in stacks:
		msg += s[-1].replace('[','').replace(']','')
	print(msg)

# part 1
file = open("input", "r")
read_s, init = True, True
for line in file:
	if init:
		stacks = [[] for _ in range(0, int(len(line)/4)) ]
		init = False

	if line.strip() == "":
		read_s = False
		for s in stacks:
			s.reverse()
	else:
		if read_s:
			for i in range(0,len(stacks)):
				content = line[i*4:i*4+3].strip()
				if content == '1':
					break
				elif content:
					stacks[i].append(content)
		else:
			_, qty, _, src, _, dest = line.strip().split(" ")
			qty = int(qty)
			src = int(src) - 1
			dest = int(dest) - 1
			while qty:
				stacks[dest].append(stacks[src].pop())
				qty -= 1
answer(stacks)

# part 2
file = open("input", "r")
read_s, init = True, True
for line in file:
	if init:
		stacks = [[] for _ in range(0, int(len(line)/4)) ]
		init = False

	if line.strip() == "":
		read_s = False
		for s in stacks:
			s.reverse()
	else:
		if read_s:
			for i in range(0,len(stacks)):
				content = line[i*4:i*4+3].strip()
				if content == '1':
					break
				elif content:
					stacks[i].append(content)
		else:
			_, qty, _, src, _, dest = line.strip().split(" ")
			qty = int(qty)
			src = int(src) - 1
			dest = int(dest) - 1
			stacks[dest] += stacks[src][-qty:]
			while qty:
				stacks[src].pop()
				qty -= 1
answer(stacks)
