#!/usr/bin/env python3
from collections import defaultdict
import itertools

# in_file = 'example'
in_file = 'input'

with open(in_file) as f:
	g = defaultdict(lambda: '.')
	lines = f.read().splitlines()
	i = 1
	for l in lines:
		j = 1
		for c in l:
			if i == 1 and c == '.':
				start = 1+j*1j
			g[i+j*1j] = c
			j += 1
		i += 1
	h,w = i-1,j-1

	for j in range(1,w+1):
		if g[h+j*1j] == '.':
			end = h+j*1j

	dirs = {'>': 1j, '<': -1j, 'v': 1, '^': -1}

def wrap(pos):
	npos = 0
	if pos.real == 1:
		npos += h-1 + pos.imag*1j
	elif pos.real == h:
		npos += 2 + pos.imag*1j
	elif pos.imag == 1:
		npos += (w-1)*1j + pos.real
	elif pos.imag == w:
		npos += 2j + pos.real
	else:
		return pos
	return npos

def round(g):
	ng = defaultdict(lambda: '.')
	for i,j in itertools.product(range(1,h+1),range(1,w+1)):
		if i == 1 or j == 1 or i == h or j == w:
			ng[i+j*1j] = '#'
	for i,j in itertools.product(range(2,h),range(2,w)):
		pos = i+j*1j
		s = list(g[pos])
		while s and (d:=s.pop()) != '.':
			npos = wrap(pos + dirs[d])
			ng[npos] += d
	ng[end]=ng[start]= '.'
	return ng

def find_path(start, end, g):
	open_list = [start]
	directions = [-1j,-1,0,1,1j]
	m = 0
	while True:
		nopen_list = []
		for node,nb in itertools.product(open_list,directions):
			next_node = node + nb
			if g[next_node] == '.' and next_node.real > 0 and next_node.imag > 0:
				if next_node == end: # arrived at destination
					return m,g
				nopen_list.append(next_node)
		open_list = list(set(nopen_list)) # uniq
		g = round(g)
		m += 1

# part 1
m,g = find_path(start,end,g)
print(m)

# part 2
r = m
m,g = find_path(end,start,g)
r += m
m,g = find_path(start,end,g)
r += m
print(r)
