#!/usr/bin/python3
file = open("input", "r")

shape_points = { 'X': 1, 'Y': 2, 'Z': 3 }
score_part1, score_part2 = 0, 0
draw, win = 3, 6
for line in file:
	them, me = line.strip().split(' ')

	score_part1 += shape_points[me]
	if them == "A": # rock
		if me == "X": # rock
			score_part1 += draw
		elif me == "Y": # paper
			score_part1 += win
	elif them == "B": # paper
		if me == "Y": # paper
			score_part1 += draw
		elif me == "Z": # scissor
			score_part1 += win
	elif them == "C": # scissor
		if me == "Z": # scissor
			score_part1 += draw
		elif me == "X": # rock
			score_part1 += win

	if them == "A": # rock
		if me == "X": # lose = scissor
			score_part2 += 0 + 3
		if me == "Y": # draw = rock
			score_part2 += 3 + 1
		if me == "Z": # win = paper
			score_part2 += 6 + 2
	elif them == "B": # paper
		if me == "X": # lose = rock
			score_part2 += 0 + 1
		if me == "Y": # draw = paper
			score_part2 += 3 + 2
		if me == "Z": # win = scissor
			score_part2 += 6 + 3
	elif them == "C": # scissor
		if me == "X": # lose = paper
			score_part2 += 0 + 2
		if me == "Y": # draw = scissor
			score_part2 += 3 + 3
		if me == "Z": # win = rock
			score_part2 += 6 + 1

print(score_part1)
print(score_part2)
