#!/usr/bin/env python3
# in_file = 'example'
in_file = 'input'

with open(in_file) as f:
	lines = f.read().splitlines()

def snafu_to_dec(x):
	x = list(x)
	c = x.pop()
	if c == '=':
		c = -2
	elif c == '-':
		c = -1
	else:
		c = int(c)
	if not len(x):
		return c
	else:
		return c + 5*snafu_to_dec(''.join(x))

def snafu_sum(a,b):
	if a in ['0','1'] and b in ['0','1']:
		s = str(int(a)+int(b))
		r = '0'
	elif a == '0':
		s = b
		r = '0'
	elif a == '1':
		if b == '=':
			s = '-'
			r = '0'
		elif b == '-':
			s = '0'
			r = '0'
		elif b == '2':
			s = '='
			r = '1'
		else:
			return snafu_sum(b,a)
	elif a == '2':
		if b == '=':
			s = '0'
			r = '0'
		elif b == '-':
			s = '1'
			r = '0'
		elif b == '2':
			s = '-'
			r = '1'
		else:
			return snafu_sum(b,a)
	elif a == '3' and b == '0':
		s = '='
		r = '1'
	elif a == '4' and b == '0':
		s = '-'
		r = '1'
	elif a == '3' and b == '1':
		s = '-'
		r = '1'
	elif a == '4' and b == '1':
		s = '0'
		r = '1'
	else:
		return snafu_sum(b,a)
	return s,r

def base5_to_snafu(x,r='0'):
	x = list(x)
	c = x.pop()
	c,r = snafu_sum(c,r)
	if not len(x):
		if r != '0':
			return r+c
		else:
			return c
	else:
		return base5_to_snafu(''.join(x),r)+c

# stolen from the Internet
def dec_to_base(num,base):
	base_num = ""
	num = int(num)
	while num>0:
		dig = int(num%base)
		if dig<10:
			base_num += str(dig)
		else:
			base_num += chr(ord('A')+dig-10)
		num //= base
	base_num = base_num[::-1]
	return str(base_num)

def dec_to_snafu(x):
	return base5_to_snafu(dec_to_base(x,5))

r = 0
for l in lines:
	r += snafu_to_dec(l)
print(dec_to_snafu(r))
