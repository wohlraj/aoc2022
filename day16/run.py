#!/usr/bin/env python3
import re

with open('input') as f:
    lines = f.read().splitlines()
    data = {}
    for l in lines:
        p = l.split(';')
        p1 = p[0]
        p2 = p[1]
        _, valve, _, _, rate = p1.split()
        dest = [d.split(',')[0] for d in p2.split()[4:]]
        rate = int(rate.split('=')[1])
        data[valve] = {'rate': rate, 'dest': dest}

def find_path(data, time_limit):
    open_list = set()
    start = "AA"
    open_list.add(start)
    pressure = {}
    pressure[start] = 0
    pressure_rate = {}
    pressure_rate[start] = 0
    time = 0
    closed_list = set()
    open_char = '#'

    while time < time_limit:
        print("-----------------")
        print(time,len(open_list),len(closed_list))

        open_list_list = list(open_list)
        for path in open_list_list:
            position = path.split('-')[-1]
            if position[-1] == open_char:
                position = position[-3:-1]

            for o in [True, False]:
                # choice: open valve
                if o:
                    if position + open_char in path:
                        continue # can't open a valve twice
                    rate = data[path.split('-')[-1]]['rate']
                    if not rate:
                        continue # checking the valve rate != 0
                    next_path = path + open_char
                    pressure_rate[next_path] = pressure_rate[path] + rate
                    if not next_path in closed_list:
                        # releasing pressure
                        pressure[next_path] = pressure[path] + pressure_rate[path]
                        open_list.add(next_path)
                # choice: move
                else:
                    for dest in data[position]['dest']:
                        next_path = path + '-' + dest
                        pressure_rate[next_path] = pressure_rate[path]
                        if not next_path in closed_list:
                            # releasing pressure
                            pressure[next_path] = pressure[path] + pressure_rate[path]
                            open_list.add(next_path)

            closed_list.add(path)

        # rewrite paths
        new_open_list = set()
        new_closed_list = set()
        for path in open_list:
            new_path = re.sub("-[A-Z]{2}-", "-", path)
            pressure[new_path] = pressure[path]
            pressure_rate[new_path] = pressure_rate[path]
            new_open_list.add(new_path)
        open_list = new_open_list
        for path in closed_list:
            new_path = re.sub("-[A-Z]{2}-", "-", path)
            new_closed_list.add(new_path)
        closed_list = new_closed_list

        # time moves on
        time += 1

    res, path = 0, None
    for p in open_list:
        r = pressure[p]
        if r > res:
            res = r
            path = p
    return res,p

print(find_path(data,30))
