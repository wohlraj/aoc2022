#!/usr/bin/env python3
import itertools
import collections as c
import functools

with open('input') as f:
    lines = f.read().splitlines()
    data = {}
    open_valves = {}
    for l in lines:
        p = l.split(';')
        p1 = p[0]
        p2 = p[1]
        _, valve, _, _, rate = p1.split()
        dest = [d.split(',')[0] for d in p2.split()[4:]]
        rate = int(rate.split('=')[1])
        data[valve] = {'rate': rate, 'dest': dest}
        if rate:
            open_valves[valve] = rate

# Floyd-Warshall, stolen from https://topaz.github.io/paste/#XQAAAQDfAgAAAAAAAAA0m0pnuFI8c82uPD0wiI6r5tRTRja98xwzlfwFtjHHMXROBlAd++OM5E2aWHrlz38tgjgBrDMkBDPm5k7eRTLnCaSEUZUXANmWw6a7dmZdD+qaJFp7E26PQ9Ml4fpikPmCeDnULBn3YHI/yLHbVDEdzTxQZhxa+aFb3fX8qpx50mBxYGkYIvkYoHqoND3JEEe2PE8yfBjpZNgC+Vp30p9nwCUTCSQrDlaj6RCgZyoOK4E/0QTzzMTpAvuwXfRpaEG4N87Y0Rr49K516SKwkvAatNXD/MBZ2thEgjpndUPRb/SA5eo0d/OjeyIlhgFibQYYZ4KHpAn3uPUJ9CSsdyr6/TnmqI95UsPYgMPNLWjLQmy3+35ne8bKXq3SHASY+91H7LIAFMGp5QhI53Qkvfo+WAJDHW6OTabv0QXSAvP57DAnBBAMS+R0W4H3bc4fRaVa+nfP7ifAKLKxGr1w3jHedPV2HRQ4bLOdmkB0vO9OReM6lNK7nTH1EF91P5PwmenHxXGnjjhp12efsEpBwFP/p/Vk7z/7zxwFT7c5+MBovbAHfbFNxQZtnVlrS1cGvRmx5bufXqoglHIp7DFNWyZVPp8TE5qiC8hSEyzLr/+x2pjq
distances = c.defaultdict(lambda: 1000)
for v1 in data:
    for v2 in data[v1]['dest']:
        distances[v1,v2] = 1
for k, i, j in itertools.product(data,data,data):
    distances[i,j] = min(distances[i,j], distances[i,k] + distances[k,j])

@functools.cache
def search(t, vs=frozenset(open_valves), u='AA', elephant=False):
    pressures = [0]
    for v in vs:
        t2 = t-distances[u,v]-1
        if t2>=0:
            # elephant's path, will open more valves which means more pressure will be released even on my path
            # appending on every loop to give the elephant a chance to not do anything in the first rounds
            if elephant:
                pressures.append(search(26, vs))
            # my path
            pressures.append(open_valves[v] * t2 + search(t2, vs-{v}, v, elephant))
    return max(pressures)

print(search(30))
print(search(26,elephant=True))
