#!/usr/bin/python3
def priority(char):
	return ord(char) - (38 if char.isupper() else 96)

file = open("input", "r")
sum = 0
for line in file:
	line = line.strip()
	mid = int(len(line)/2)
	left = line[0:mid]
	right = line[mid::]
	for item in left:
		if item in right:
			sum += priority(item)
			break
print(sum)

file = open("input", "r")
sum, i, group = 0, 0, []
for line in file:
	line = line.strip()
	group.append([*line])
	i += 1
	if i == 3: # next group
		i = 0
		badge = set(group[0]).intersection(group[1]).intersection(group[2])
		group = []
		sum += priority(','.join(str(c) for c in badge))
print(sum)
