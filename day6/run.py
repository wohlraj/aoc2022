#!/usr/bin/python3
def solve(n):
    with open('input') as f:
        lines = f.read().splitlines()
        for line in lines:
            for i in range(0, len(line)-n):
                if len(set(line[i:i+n])) == n:
                    print(i+n)
                    break
solve(4)
solve(14)
