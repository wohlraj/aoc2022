#!/usr/bin/python3
import math

def solve(nbr_rounds,worry_level_divided):
    # using least common multiple to keep worry levels manageable
    lcm = 1

    monkeys = []
    with open('input') as f:
        lines = f.read().splitlines()
        for line in lines:
            if 'Monkey' in line:
                monkey = {}
                monkey['id'] = int(line.split(':')[0][-1])
                monkey['inspections'] = 0
            elif 'items' in line:
                monkey['items'] =[int(el.strip()) for el in line.split(':')[1].strip().split(',')]
            elif 'Operation' in line:
                monkey['operation'] = line.split(' ')[-2]
                monkey['operation_val'] = line.split(' ')[-1]
            elif 'Test' in line:
                monkey['test_val'] = int(line.split(' ')[-1])
                lcm *= monkey['test_val']
            elif 'If true' in line:
                monkey['test_true'] = int(line.split(' ')[-1])
            elif 'If false' in line:
                monkey['test_false'] = int(line.split(' ')[-1])
                monkeys.append(monkey)

    for r in range(nbr_rounds):
        for mk in monkeys:
            for mi in mk['items']:
                mk['inspections'] += 1

                if mk['operation_val'] == 'old':
                    operation_val = mi
                else:
                    operation_val = int(mk['operation_val'])
                if mk['operation'] == '*':
                    mi *= operation_val
                if mk['operation'] == '+':
                    mi += operation_val

                if worry_level_divided:
                    mi = math.floor(mi/3)

                if mi%mk['test_val']:
                    next_mk = mk['test_false']
                else:
                    next_mk = mk['test_true']
                monkeys[next_mk]['items'].append(mi%lcm)
                monkeys[mk['id']]['items'] = monkeys[mk['id']]['items'][1::]

    inspections = []
    for mk in monkeys:
        inspections.append(mk['inspections'])
    inspections.sort()
    print(inspections[-2]*inspections[-1])

solve(20, True)
solve(10000, False)
