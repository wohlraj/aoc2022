#!/usr/bin/env python3

with open('input') as f:
    grid = [[el for el in l] for l in f.read().splitlines()]
    for i in range(len(grid)):
        for j in range(len(grid[0])):
            if grid[i][j] == 'S':
                start = tuple([i,j])
                grid[i][j] = 1
            elif grid[i][j] == 'E':
                end = tuple([i,j])
                grid[i][j] = 26
            else:
                grid[i][j] = ord(grid[i][j]) - ord('a') + 1

def find_path(start, end, grid):
    open_list = set()
    open_list.add(start)
    closed_list = set()
    parents = {}
    parents[start] = start
    dists = {}
    dists[start] = 0

    while len(open_list):
        # considering node in open_list with shortest path
        node = None
        for n in open_list:
            if node == None or dists[n] < dists[node]:
                node = n

        for nb in [[0,1],[1,0],[0,-1],[-1,0]]:
            next_node = tuple([node[0]+nb[0],node[1]+nb[1]])
            if (next_node[0] >= 0 and next_node[0] < len(grid)) and (next_node[1] >= 0 and next_node[1] < len(grid[0])):
                if grid[next_node[0]][next_node[1]] - grid[node[0]][node[1]] > 1: # allowed path
                    continue
                elif next_node == end: # arrived at destination
                    return dists[node] + 1
                elif not next_node in closed_list:
                    open_list.add(next_node)
                    parents[next_node] = node
                    dists[next_node] = dists[node] + 1
        open_list.remove(node)
        closed_list.add(node)

# part 1
print(find_path(start, end, grid))

# part 2
starts = []
paths = []
for i in range(len(grid)):
    for j in range(len(grid[0])):
        # starting on borders is not an explicit requirement, but it works and speeds up calculation
        if grid[i][j] == 1 and (i == 0 or j == 0 or i == len(grid)-1 or j == len(grid[0])-1):
            starts.append(tuple([i,j]))
for start in starts:
    path = find_path(start, end, grid)
    if path:
        paths.append(path)
print(min(paths))
