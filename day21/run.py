#!/usr/bin/env python3
import sympy

# in_file = 'example'
in_file = 'input'

d = {}
with open(in_file) as f:
	for n,fn in [x.split(':') for x in f.read().splitlines()]:
		fn = fn.strip()
		d[n] = fn

# part 1
def decode(key):
	# print("decoding",key)
	try:
		int(key)
	except:
		pass
	else:
		return key
	a,op,b = key.split()
	return int(eval(str(decode(d[a]))+op+str(decode(d[b]))))
print(decode(d['root']))

# part 2
def values(lines):
	d = {}
	for l in lines:
		a,b = l.split(':')
		b = b.strip()
		try:
			int(b)
		except:
			pass
		else:
			d[a]=b
	return d

def replace(lines, values):
	nlines = []
	for l in lines:
		if 'root' in l:
			nlines.append(l)
		for k,v in values.items():
			if k in l:
				nlines.append(l.replace(k,v))
	nnlines = []
	for l in nlines:
		a,b = l.split(':')
		b = b.strip()
		if not a == b:
			nnlines.append(l)
	return nnlines

def extract_vars(line):
	res = []
	for el in line.split():
		el = el.strip(':').strip('(').strip(')')
		if el != 'root' and len(el) == 4 and not el[0] in '012356789':
			res.append(el)
	return res

def extract_equation(var,line):
	a,b = line.split(':')
	if var == a:
		return b.strip()
	else:
		c,op,d = b.split()
		if var == c:
			if op == '+':
				return a+' - '+d
			elif op == '-':
				return a+' + '+d
			elif op == '*':
				return a+' / '+d
			elif op == '/':
				return a+' * '+d
		if var == d:
			if op == '+':
				return a+' - '+c
			elif op == '-':
				return c+' - '+a
			elif op == '*':
				return a+' / '+c
			elif op == '/':
				return c+' / '+a

def find_equation(var,lines):
	for l in lines:
		if var in l and not 'root' in l:
			return l

# init
with open(in_file) as f:
	d = f.read().splitlines()
	nd = []
	for l in d:
		if not 'humn:' in l: # dropping humn
			if 'root' in l: # replacing root = a + b by root = a - b
				a,_,b = l.split(':')[1].strip().split(' ')
				nd.append('root: '+a+' - '+b)
			else:
				nd.append(l)
	d = nd
vals = values(d)

# first round
def round(d):
	for l in d:
		if 'root' in l:
			for var in extract_vars(l):
				if var == 'humn':
					continue
				if var in vals.keys():
					d.remove(l)
					d.append(l.replace(var,vals[var]))
				else:
					eq = find_equation(var,d)
					d.remove(eq)
					eq = extract_equation(var,eq)
					d.remove(l)
					d.append(l.replace(var,'('+eq+')'))
				break
			break
	return d

while len(d)-len(vals)>1:
	round(d)
	while len(extract_vars(d[-1]))>1: # replacing last values
		round(d)
sympy_eq = sympy.sympify("Eq(" + d[-1].replace('humn','x').split(':')[1] + ", 0 )")
print(sympy.solve(sympy_eq)[0])
