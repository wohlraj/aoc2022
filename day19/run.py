#!/usr/bin/env python3
import re

# in_file = 'example'
in_file = 'input'

with open(in_file) as f:
	bp = [] # blueprints
	for l in f.read().splitlines():
		r = r'Blueprint (\d+): Each ore robot costs (\d+) ore. Each clay robot costs (\d+) ore. Each obsidian robot costs (\d+) ore and (\d+) clay. Each geode robot costs (\d+) ore and (\d+) obsidian.'
		for _,a,b,c,d,e,f in re.findall(r, l):
			nbp = {'or': {}, 'cl': {}, 'ob': {}, 'ge': {}}
			nbp['or']['or'] = int(a)
			nbp['cl']['or'] = int(b)
			nbp['ob']['or'] = int(c)
			nbp['ob']['cl'] = int(d)
			nbp['ge']['or'] = int(e)
			nbp['ge']['ob'] = int(f)
			bp.append(nbp)

def round(robots, ores, queue=[]):
	# step 2: collect resources from robots
	for ro,re in robots.items(): # robot, resource
		ores[ro] += re
	# step 3: collect robots from factory
	if queue:
		robots[queue.pop()] += 1
	return robots, ores

def buy(costs, ores):
	for re,co in costs.items(): # resource, cost
		if not ores[re] >= co:
			return None
		else:
			ores[re] -= co
	return ores

# part 1
mt = 24 # max time
answer = 0
for idx,bpi in enumerate(bp): # loop over blueprints
	robots = {'or': 1, 'cl': 0, 'ob': 0, 'ge': 0}
	ores = {'or': 0, 'cl': 0, 'ob': 0, 'ge': 0}
	states_queue = [(robots, ores)]
	for t in range(1,mt+1):
		nstates_queue = []
		while states_queue:
			robots, ores = states_queue.pop()
			# step 1: explore all buying possiblities
			# buying robots
			for ro in ores.keys():
				if nores:=buy(bpi[ro],ores.copy()):
					nstates_queue.append(round(robots.copy(), nores.copy(), [ro]))
			# not buying anything
			nstates_queue.append(round(robots.copy(), ores.copy()))
		# heuristics: sorting states by robot and its associated resource, then pruning 500+
		states_queue = sorted(nstates_queue,key=lambda a:(a[1]['ge'],a[0]['ge'],a[1]['ob'],a[0]['ob'],a[1]['cl'],a[0]['cl'],a[1]['or'],a[0]['or']),reverse=True)[:500]
	res = 0
	for state in states_queue:
		res = max(res, state[1]['ge'])
	answer += (idx+1)*res
print(answer)

# part 2
mt = 32 # max time
answer = 1
for idx,bpi in enumerate(bp[0:3]): # loop over blueprints
	robots = {'or': 1, 'cl': 0, 'ob': 0, 'ge': 0}
	ores = {'or': 0, 'cl': 0, 'ob': 0, 'ge': 0}
	states_queue = [(robots, ores)]
	for t in range(1,mt+1):
		nstates_queue = []
		while states_queue:
			robots, ores = states_queue.pop()
			# step 1: explore all buying possiblities
			# buying robots
			for ro in ores.keys():
				if nores:=buy(bpi[ro],ores.copy()):
					nstates_queue.append(round(robots.copy(), nores.copy(), [ro]))
			# not buying anything
			nstates_queue.append(round(robots.copy(), ores.copy()))
		# heuristics: sorting states by robot and its associated resource, then pruning 500+
		states_queue = sorted(nstates_queue,key=lambda a:(a[1]['ge'],a[0]['ge'],a[1]['ob'],a[0]['ob'],a[1]['cl'],a[0]['cl'],a[1]['or'],a[0]['or']),reverse=True)[:10000]
	res = 0
	for state in states_queue:
		res = max(res, state[1]['ge'])
	answer *= res
print(answer)
