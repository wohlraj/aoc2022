#!/usr/bin/env python3
grid_height = 200
grid_width = 1000
char_empty = '_'
char_rock = '#'
lowest_point = 0
grid = [[char_empty for _ in range(grid_height)] for _ in range(grid_width)]

with open('input') as f:
    for line in f.read().splitlines():
        positions = line.split(' -> ')
        for i in range(len(positions)-1):
            x1,y1 = positions[i].split(',')
            x1,y1 = [int(x1),int(y1)]
            x2,y2 = positions[i+1].split(',')
            x2,y2 = [int(x2),int(y2)]
            if x1 == x2:
                ymin,ymax = min(y1,y2),max(y1,y2)
                for y in range(ymin,ymax+1):
                    grid[x1][y] = char_rock
                    lowest_point = max(y, lowest_point)
            else:
                xmin,xmax = min(x1,x2),max(x1,x2)
                for x in range(xmin,xmax+1):
                    grid[x][y1] = char_rock
    lowest_point += 2

def display(xmin, xmax):
    header_width = xmax-xmin-8
    header = ''.join(['.'*header_width])
    print(" ",str(xmin),str(header),str(xmax))
    for y in range(lowest_point+1):
        line = str(y)+" "
        for x in range(xmin,xmax):
            line += grid[x][y]
        print(line)

def pop_sand(position):
    x,y = position
    # if reached bottom or if the heap is full
    if y == lowest_point or grid[x][y] == 'o':
        return False
    grid[x][y] = 'o'
    for x2 in [x,x-1,x+1]:
        if grid[x2][y+1] == char_empty:
            grid[x][y] = char_empty
            return pop_sand([x2,y+1])
    return True

# part 1
res = 0
while pop_sand([500,0]):
    res += 1
# display(480, 550)
print(res)

# part 2
for i in range(len(grid)):
    grid[i][lowest_point] = char_rock

while pop_sand([500,0]):
    res += 1
# display(480, 550)
print(res)
