#!/usr/bin/env python3
in_file = 'example'
# in_file = 'input'

with open(in_file) as f:
	lines = f.read().splitlines()

def reduce(line):
	identical = False
	while not identical:
		nline = line.replace('()','')
		nline = nline.replace('<>','')
		nline = nline.replace('[]','')
		nline = nline.replace('{}','')
		identical = nline == line
		line = nline
	return nline

nlines = [reduce(l) for l in lines]
print("\n".join(nlines))
