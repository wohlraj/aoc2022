#!/usr/bin/env python3
from collections import defaultdict
import re

# in_file = 'example'
in_file = 'input'

w = 0
with open(in_file) as f:
	g = defaultdict(lambda: ' ')
	i = 0
	start = None
	for l in f.read().splitlines():
		if l != "" and not 'R' in l:
			j = 0
			for c in l:
				g[i,j] = c
				if not start and c == '.':
					start = (i,j)
				j += 1
		else:
			dirs = re.findall(r'(\d+|R|L)',l)
		w = max(w,i)
		i += 1
h = i-1

w -= 1
h -= 1

def find_edge(pos,facing,part2=False):
	i,j = pos
	if part2:
		if facing == 0:
			if i < 50: # yellow
				i = 149-i
				j = 99
				facing = 2
			elif i < 100: # dark blue
				j = i+50
				i = 49
				facing = 3
			elif i < 150: # yellow
				i = 149-i
				j = 149 
				facing = 2
			elif i < 200: # green
				j = i-100
				i = 149
				facing = 3
		elif facing == 1:
			if j < 50: # brown
				j = j + 100
				i = 0
			elif j < 100: # green
				i = j + 100
				j = 49
				facing = 2
			elif j < 150: # dark blue
				i = j - 50
				j = 99
				facing = 2
		elif facing == 2:
			if i < 50: # grey
				i = 149-i
				j = 0
				facing = 0
			elif i < 100: # red
				j = i-50
				i = 100
				facing = 1
			elif i < 150: # grey
				j = 50
				i = 149-i
				facing = 0
			elif i < 200: # cyan
				j = i-100
				i = 0
				facing = 1
		elif facing == 3:
			if j < 50: # red
				i = j+50
				j = 50
				facing = 0
			elif j < 100: # cyan
				i = j+100
				j = 0
				facing = 0
			elif j < 150: # brown
				i = 199
				j = j-100
	else:
		if facing == 0:
			j = 0
			while g[i,j] == ' ':
				j += 1
		elif facing == 1:
			i = 0
			while g[i,j] == ' ':
				i += 1
		elif facing == 2:
			j = w+1
			while g[i,j] == ' ':
				j -= 1
		elif facing == 3:
			i = h+1
			while g[i,j] == ' ':
				i -= 1
		if g[i,j] == '#':
			return None,None
	return [(i,j),facing]

def move(pos,facing,part2=False):
	i,j = pos
	if facing == 0:
		npos = (i,j+1)
		if g[npos] == ' ':
			pos, facing = find_edge(npos,facing,part2)
		else:
			pos = npos
	elif facing == 1:
		npos = (i+1,j)
		if g[npos] == ' ':
			pos, facing = find_edge(npos,facing,part2)
		else:
			pos = npos
	elif facing == 2:
		npos = (i,j-1)
		if g[npos] == ' ':
			pos, facing = find_edge(npos,facing,part2)
		else:
			pos = npos
	elif facing == 3:
		npos = (i-1,j)
		if g[npos] == ' ':
			pos, facing = find_edge(npos,facing,part2)
		else:
			pos = npos

	if g[pos] == '#':
		return None,None
	return [pos, facing]

def solve(part2=False):
	pos = start
	facing = 0
	for d in dirs:
		if d == 'R':
			facing += 1
		elif d == 'L':
			facing -=1
		else:
			k = int(d)
			while k:
				npos, nfacing = move(pos,facing,part2)
				if npos:
					i,j = pos
					pos, facing = npos, nfacing
				else:
					break
				k -= 1
		facing = facing%4
	i,j = pos
	print((i+1)*1000+(j+1)*4+facing)

solve()
solve(True)
