#!/usr/bin/python3

file = open("input", "r")
count_part1 = 0
count_part2 = 0
for line in file:
	left, right = line.strip().split(',')
	left_l = [int(x) for x in left.split('-')]
	right_l = [int(x) for x in right.split('-')]
	left_r = range(left_l[0], left_l[1] + 1)
	right_r = range(right_l[0], right_l[1] + 1)
	m1 = min(left_l[0], right_l[0])
	m2 = max(left_l[1], right_l[1]) + 1
	if range(m1, m2) in (left_r, right_r):
		count_part1 += 1
	if len(set(left_r).intersection(right_r)) != 0:
		count_part2 += 1
print(count_part1)
print(count_part2)
