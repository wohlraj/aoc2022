#!/usr/bin/env python3
from collections import defaultdict
import itertools

# in_file = 'example'
in_file = 'input'

g = defaultdict(lambda: '.')
e = 0
with open(in_file) as f:
	lines = f.read().splitlines()
	i = 0
	for l in lines:
		j = 0
		for c in l:
			if c == '#':
				g[i,j] = '#_'+str(e) # cells are '#_e'
				e += 1
			j += 1
		i += 1

dirs_n = [(-1,-1),(-1,0),(-1,1)]
dirs_s = [(1,-1),(1,0),(1,1)]
dirs_w = [(1,-1),(0,-1),(-1,-1)]
dirs_e = [(-1,1),(0,1),(1,1)]
dirs = [dirs_n, dirs_s, dirs_w, dirs_e] # 0, 1, 2, 3
all_dirs = dirs_n+dirs_s+dirs_w+dirs_e
moves = [dirs_n[1], dirs_s[1], dirs_w[1], dirs_e[1]]

def rotate_dirs():
	global dirs, moves
	dirs = dirs[1::]+[dirs[0]]
	moves = moves[1::]+[moves[0]]

def score():
	i_min = 100
	i_max = -100
	j_min = 100
	j_max = -100
	for k in range(e):
		vals = list(g.values())
		keys = list(g.keys())
		idx = vals.index('#_'+str(k))
		pos = keys[idx]
		i_min = min(i_min, pos[0])
		i_max = max(i_max, pos[0])
		j_min = min(j_min, pos[1])
		j_max = max(j_max, pos[1])
	score = 0
	for i,j in itertools.product(range(i_min,i_max+1),range(j_min,j_max+1)):
		if g[i,j] == '.':
			score += 1
	return score

def round():
	no_move = True
	for k in range(e): # first half
		idx = list(g.values()).index('#_'+str(k))
		i,j = list(g.keys())[idx]
		move = 0
		# if no other elf is around, not moving at all
		if all([ '##' in g[i+dd[0],j+dd[1]] or '.' in g[i+dd[0],j+dd[1]] for dd in all_dirs]):
			continue
		# else, considering directions
		for d in dirs:
			if not all([ '##' in g[i+dd[0],j+dd[1]] or '.' in g[i+dd[0],j+dd[1]] for dd in d]):
				move += 1
			else:
				break
		if move < 4: # not exhausted directions
			ni,nj = i+moves[move][0],j+moves[move][1]
			if '##' in g[ni,nj]:
				g[ni,nj] = '.' # cancelling other elve's move
			else:
				g[ni,nj] = '#'+g[i,j] # moving, new cell is '##_e'
				no_move = False

	for k in range(e): # second half, dropping '#_e' when '##_e' exists
		if '##_'+str(k) in g.values():
			idx = list(g.values()).index('#_'+str(k))
			key = list(g.keys())[idx]
			g[key] = '.'
	for k in range(e): # second half, replacing '##_e' by '#_e'
		if '##_'+str(k) in g.values():
			idx = list(g.values()).index('##_'+str(k))
			key = list(g.keys())[idx]
			g[key] = g[key][1::]
	rotate_dirs()
	return no_move

i = 0
while i<10:
	round()
	i += 1
print(score())

done = False
while not done:
	done = round()
	i += 1
print(i)
